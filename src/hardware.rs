use crate::mmio::{self, mmio_read};

pub mod clock_and_reset;
pub mod gpio;
pub mod i2c;
pub mod pinmux;
pub mod security_engine;
pub mod tmr;
pub mod uart;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum HardwareType {
    Icosa = 0,
    Copper = 1,
    Invalid = 15,
}

impl HardwareType {
    pub fn read_from_fuse(fuse_base: usize) -> Self {
        let odm4 = mmio_read(fuse_base, mmio::fuse::CACHE_FUSE_RESERVED_ODM4);
        let hwtype1: bool = odm4 & (1 << 2) != 0;
        let hwtype2: bool = odm4 & (1 << 8) != 0;

        match (hwtype1, hwtype2) {
            (true, false) => Self::Icosa,
            (false, true) => Self::Copper,
            _ => Self::Invalid,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum HardwareState {
    Dev,
    Prod,
    Invalid,
}

impl HardwareState {
    pub fn read_from_fuse(fuse_base: usize) -> Self {
        let odm4 = mmio_read(fuse_base, mmio::fuse::CACHE_FUSE_RESERVED_ODM4);
        let hwstate1 = odm4 & 0b11;
        let hwstate2: bool = odm4 & (1 << 9) != 0;

        match (hwstate1, hwstate2) {
            (3, false) => Self::Dev,
            (0, true) => Self::Prod,
            _ => Self::Invalid,
        }
    }
}
