use core::arch::asm;

use crate::{
    dc_isw, dsb_ish,
    hardware::tmr::read_usec_cntr,
    ic_iallu, isb,
    mmio::{self, mmio_or, mmio_read, mmio_write, pmc::APBDEV_PMC_SCRATCH200},
    read_spr, write_spr,
};

pub fn reboot_to_rcm(pmc_base: Option<usize>, flow_controller_base: Option<usize>) {
    let pmc_base = pmc_base.unwrap_or(crate::mmio::pmc::PMC_BASE);
    let flow_controller_base =
        flow_controller_base.unwrap_or(crate::mmio::flow_controller::FLOW_CONTROLLER_BASE);

    mmio_write(pmc_base, mmio::pmc::APBDEV_PMC_SCRATCH0, 1 << 1);
    mmio_or(
        pmc_base,
        mmio::pmc::APBDEV_PMC_CNTRL,
        mmio::pmc::PMC_CNTRL_MAIN_RST,
    );
    mmio_write(
        flow_controller_base,
        mmio::flow_controller::FLOW_CTLR_HALT_COP_EVENTS,
        mmio::flow_controller::HALT_COP_JTAG | mmio::flow_controller::HALT_COP_WAIT_EVENT,
    );
}

#[inline(always)]
pub fn cpuid() -> u8 {
    let mut mpidr: u64;

    unsafe {
        asm!(
            "MRS {output}, MPIDR_EL1",
            output = out(reg) mpidr
        );
    }

    (mpidr & 0xFF) as u8
}

#[inline(always)]
pub fn invalidate_unified_and_data_caches() {
    let clidr_el1 = read_spr!("CLIDR_EL1");
    let cache_coherancy_level = (clidr_el1 >> 0x18) & 0b111;

    for current_level in 0..cache_coherancy_level {
        let new_level_shifted = current_level << 1;
        write_spr!("CSSELR_EL1", new_level_shifted);
        isb!();

        let current_cache_size_id = read_spr!("CCSIDR_EL1");
        let associativity = ((current_cache_size_id >> 3) & 0x3FF) as u32;
        let line_size = (current_cache_size_id & 0b111) + 4;
        let number_of_sets_in_cache = ((current_cache_size_id >> 13) & 0x7FFF) + 1;

        let leading_zeros_of_associativity = (associativity + 1).leading_zeros() + 1;

        for current_associativity in 0..associativity {
            let shifted_val = ((current_associativity << leading_zeros_of_associativity) as u64)
                | new_level_shifted;
            for iter in 0..number_of_sets_in_cache {
                dc_isw!((iter << line_size) | shifted_val);
            }
        }
    }

    dsb_ish!();
    ic_iallu!();
    dsb_ish!();
    isb!();
}

#[inline(always)]
pub fn clear_and_invalidate_cache_for_address_range(start: u64, length: u64) {
    let end = align_up(start + length as u64, 0x40);
    let mut ptr = start;
    while ptr < end {
        dc_isw!(ptr);
        ptr += 0x40;
    }
}

#[inline(always)]
pub const fn align_up(value: u64, alignment: u64) -> u64 {
    (value | (alignment - 1)) + 1
}

pub fn usleep(tmr_base: usize, usecs: u32) {
    let end = read_usec_cntr(tmr_base) + usecs;
    while read_usec_cntr(tmr_base) < end {}
}

pub fn endian_flip(input: u32) -> u32 {
    ((input & 0x0000_00FF) << (3 * 8))
        | ((input & 0x0000_FF00) << 8)
        | ((input & 0x00FF_0000) >> 8)
        | ((input & 0xFF00_0000) >> (3 * 8))
}

pub fn align_buffer(input: &mut [u8], alignment: u64, needed_size: u64) -> &mut [u8] {
    let start_offset =
        (align_up(input.as_ptr() as u64, alignment) - input.as_ptr() as u64) as usize;
    &mut input[start_offset..(start_offset + needed_size as usize)]
}

#[inline(always)]
pub fn translate_address<const LMASK: u64, T>(input: *const T) -> u32 {
    let at_output: u64;
    unsafe {
        asm!(
            "AT S1E3R, {0}",
            "MRS {1}, PAR_EL1",
            in(reg) input,
            out(reg) at_output
        )
    }

    ((at_output & 0xfffff000) | (input as u64 & LMASK)) as u32
}

#[no_mangle]
#[inline(always)]
pub extern "C" fn set_pmc_scratch_200_if_unset(pmc_base: usize, value: u32) {
    if mmio_read(pmc_base, APBDEV_PMC_SCRATCH200) == 0 {
        mmio_write(pmc_base, APBDEV_PMC_SCRATCH200, value);
    }
}

pub const fn colour_to_scratch_contents(red: u8, green: u8, blue: u8, error_code: u32) -> u32 {
    let red = red / 85;
    let green = green / 85;
    let blue = blue / 85;

    ((red as u32) << 28)
        | ((green as u32) << 24)
        | ((blue as u32) << 20)
        | (error_code & 0b11111111111111111111)
}
