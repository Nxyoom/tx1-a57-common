use crate::hardware::{self, uart::Uart};

pub fn init(
    gpio_base: usize,
    pinmux_base: usize,
    car_base: usize,
    uart_base: usize,
    tmr_base: usize,
) {
    hardware::gpio::init(
        gpio_base,
        hardware::gpio::Port::D,
        hardware::gpio::Pin::One as u32,
        hardware::gpio::Mode::Spio,
    );
    hardware::pinmux::init_uart(pinmux_base, Uart::C);
    hardware::clock_and_reset::UART_C_CAR_DEVICE.initialise(car_base);
    hardware::uart::init(uart_base, tmr_base, Uart::C, 115200);
}

pub fn write(uart_base: usize, msg: &str) {
    hardware::uart::write(uart_base, Uart::C, msg.as_bytes());
}
