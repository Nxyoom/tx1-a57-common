use crate::{
    mmio::{self, mmio_read, mmio_write},
    utils::usleep,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Uart {
    A = 0x000,
    B = 0x040,
    C = 0x200,
    D = 0x300,
}

pub fn init(uart_base: usize, tmr_base: usize, uart: Uart, baud: u32) {
    let base = uart_base + uart as usize;
    let baud_divisor = (408000000 + (baud * 8)) / (baud * 0x10);

    // Enable programming of baud div regs
    mmio_write(base, mmio::uart::LCR, 1 << 7);

    // Set baud divisor LSB
    mmio_write(base, mmio::uart::THR_DLAB, baud_divisor & 0xff);
    // Set baud divisor MSB
    mmio_write(base, mmio::uart::IER_DLAB, (baud_divisor >> 8) & 0xff);

    // Disable programming of baud div regs
    mmio_write(base, mmio::uart::LCR, 0);

    // Disable all interrupts
    mmio_write(base, mmio::uart::IER_DLAB, 0);
    // Clear RX TX FIFO, enable FIFO
    mmio_write(base, mmio::uart::IIR_FCR, (1 << 2) | (1 << 1) | 1);

    mmio_read(base, mmio::uart::LSR);

    usleep(tmr_base, ((baud + 999999) / baud) * 3);

    // Set word size to 8 bits
    mmio_write(base, mmio::uart::LCR, 0b11);

    mmio_write(base, mmio::uart::MCR, 0);
    mmio_write(base, mmio::uart::MSR, 0);
    mmio_write(base, mmio::uart::IDRA_CSR, 0);
    mmio_write(base, mmio::uart::RX_FIFO_CFG, 1);
    mmio_write(base, mmio::uart::MIE, 0);
    mmio_write(base, mmio::uart::ASR, 0);
}

pub fn wait_tx_fifo_not_full(base: usize, uart: Uart) {
    while mmio_read(base + uart as usize, mmio::uart::LSR) & 1 << 8 != 0 {}
}

pub fn write(base: usize, uart: Uart, buffer: &[u8]) {
    let adj_base = base + uart as usize;
    for x in buffer {
        wait_tx_fifo_not_full(base, uart);

        let byte = *x;

        if byte == 0xa {
            mmio_write(adj_base, mmio::uart::THR_DLAB, 0xd);
            wait_tx_fifo_not_full(base, uart);
        }

        mmio_write(adj_base, mmio::uart::THR_DLAB, byte as u32);
    }
}
