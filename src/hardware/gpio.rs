use core::ops::BitOr;

use crate::mmio::{self, mmio_and, mmio_or, mmio_read};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Port {
    A = 0x00,
    B = 0x04,
    C = 0x08,
    D = 0x0C,
    E = 0x100,
    F = 0x104,
    G = 0x108,
    H = 0x10C,
    I = 0x200,
    J = 0x204,
    K = 0x208,
    L = 0x20C,
    M = 0x300,
    N = 0x304,
    O = 0x308,
    P = 0x30C,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Pin {
    Zero = 1 << 0,
    One = 1 << 1,
    Two = 1 << 2,
    Three = 1 << 3,
    Four = 1 << 4,
    Five = 1 << 5,
    Six = 1 << 6,
    Seven = 1 << 7,
}

impl BitOr<Pin> for Pin {
    type Output = u32;

    fn bitor(self, rhs: Pin) -> Self::Output {
        self as u32 | rhs as u32
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Mode {
    Spio,
    Gpio,
}

pub fn init(base: usize, port: Port, pins: u32, mode: Mode) {
    let reg = mmio::gpio::CNF + port as usize;

    match mode {
        Mode::Spio => mmio_and(base, reg, !pins),
        Mode::Gpio => mmio_or(base, reg, pins),
    }

    mmio_read(base, reg);
}
