use crate::mmio::{clock_and_reset, mmio_and, mmio_or, mmio_read, mmio_write};

pub struct CARDeviceDefinition {
    pub reset_reg_off: usize,
    pub enable_reg_off: usize,
    pub source_reg_off: Option<usize>,
    pub register_bitshift: u8,
    pub clock_src: u8,
    pub clock_div: u8,
}

impl CARDeviceDefinition {
    pub const fn new(
        reset_reg_off: usize,
        enable_reg_off: usize,
        source_reg_off: Option<usize>,
        register_bitshift: u8,
        clock_src: u8,
        clock_div: u8,
    ) -> Self {
        Self {
            reset_reg_off,
            enable_reg_off,
            source_reg_off,
            register_bitshift,
            clock_src,
            clock_div,
        }
    }

    pub fn put_in_reset(&self, base: usize) {
        mmio_or(base, self.reset_reg_off, 1 << self.register_bitshift);
    }

    pub fn enable(&self, base: usize) {
        mmio_or(base, self.enable_reg_off, 1 << self.register_bitshift);
    }

    pub fn disable(&self, base: usize) {
        mmio_and(base, self.enable_reg_off, !(1 << self.register_bitshift));
    }

    pub fn bring_out_of_reset(&self, base: usize) {
        mmio_and(base, self.reset_reg_off, !(1 << self.register_bitshift));
    }

    pub fn configure_source(&self, base: usize) {
        if let Some(source) = self.source_reg_off {
            mmio_write(
                base,
                source,
                (self.clock_div as u32) | ((self.clock_src as u32) << 0x1d),
            )
        }
    }

    pub fn initialise(&self, base: usize) {
        self.put_in_reset(base);
        self.disable(base);
        self.configure_source(base);
        self.enable(base);
        self.bring_out_of_reset(base);
    }
}

// Undocumented in the TRM.
// Gathered via trial and error of CAR struct definitions in the secmon as it's present even though unsused (Thanks N).
pub const SE_CAR_DEVICE: CARDeviceDefinition = CARDeviceDefinition::new(
    clock_and_reset::RST_DEVICES_V,
    clock_and_reset::CLOCK_OUT_ENABLE_V,
    Some(clock_and_reset::CLOCK_SOURCE_SE),
    0x1f,
    0,
    0,
);

pub const UART_A_CAR_DEVICE: CARDeviceDefinition = CARDeviceDefinition::new(
    clock_and_reset::RST_DEVICES_L,
    clock_and_reset::CLOCK_OUT_ENABLE_L,
    Some(clock_and_reset::CLOCK_SOURCE_UART_A),
    0x06,
    0,
    0,
);

pub const UART_B_CAR_DEVICE: CARDeviceDefinition = CARDeviceDefinition::new(
    clock_and_reset::RST_DEVICES_L,
    clock_and_reset::CLOCK_OUT_ENABLE_L,
    Some(clock_and_reset::CLOCK_SOURCE_UART_B),
    0x07,
    0,
    0,
);

pub const UART_C_CAR_DEVICE: CARDeviceDefinition = CARDeviceDefinition::new(
    clock_and_reset::RST_DEVICES_H,
    clock_and_reset::CLOCK_OUT_ENABLE_H,
    Some(clock_and_reset::CLOCK_SOURCE_UART_C),
    0x17,
    0,
    0,
);

pub const UART_D_CAR_DEVICE: CARDeviceDefinition = CARDeviceDefinition::new(
    clock_and_reset::RST_DEVICES_U,
    clock_and_reset::CLOCK_OUT_ENABLE_U,
    Some(clock_and_reset::CLOCK_SOURCE_UART_D),
    0x01,
    0,
    0,
);

pub fn set_misc_clk_enb(base: usize, enb: bool) {
    let orig = mmio_read(base, clock_and_reset::MISC_CLK_ENB);
    let new_val = (orig & 0xefffffff) | ((enb as u32) << 0x1c);
    mmio_write(base, clock_and_reset::MISC_CLK_ENB, new_val);
}
