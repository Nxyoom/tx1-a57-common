use crate::{
    mmio::{mmio_read, mmio_write, tmr},
    utils::cpuid,
};

#[inline(always)]
pub fn read_usec_cntr(tmr_base: usize) -> u32 {
    mmio_read(tmr_base, tmr::TMR_USEC_CNTR)
}

pub fn full_system_reset_via_watchdog(tmr_base: usize) -> ! {
    let cpuid = cpuid();

    match cpuid & 0b111 {
        0 => {
            mmio_write(
                tmr_base,
                tmr::WDT_0_UNLOCK_PATTERN,
                tmr::WDT_UNLOCK_PATTERN_VALUE,
            );
            mmio_write(tmr_base, tmr::WDT_0_COMMAND, 2);
            mmio_write(tmr_base, tmr::TMR_5_PTV, (1 << 31) | (1 << 30));
            mmio_write(tmr_base, tmr::WDT_0_CONFIG, (1 << 15) | (1 << 4) | 5);
            mmio_write(tmr_base, tmr::WDT_0_COMMAND, 1);
        }
        1 => {
            mmio_write(
                tmr_base,
                tmr::WDT_1_UNLOCK_PATTERN,
                tmr::WDT_UNLOCK_PATTERN_VALUE,
            );
            mmio_write(tmr_base, tmr::WDT_1_COMMAND, 2);
            mmio_write(tmr_base, tmr::TMR_6_PTV, (1 << 31) | (1 << 30));
            mmio_write(tmr_base, tmr::WDT_1_CONFIG, (1 << 15) | (1 << 4) | 6);
            mmio_write(tmr_base, tmr::WDT_1_COMMAND, 1);
        }
        2 => {
            mmio_write(
                tmr_base,
                tmr::WDT_2_UNLOCK_PATTERN,
                tmr::WDT_UNLOCK_PATTERN_VALUE,
            );
            mmio_write(tmr_base, tmr::WDT_2_COMMAND, 2);
            mmio_write(tmr_base, tmr::TMR_7_PTV, (1 << 31) | (1 << 30));
            mmio_write(tmr_base, tmr::WDT_2_CONFIG, (1 << 15) | (1 << 4) | 7);
            mmio_write(tmr_base, tmr::WDT_2_COMMAND, 1);
        }
        3 => {
            mmio_write(
                tmr_base,
                tmr::WDT_3_UNLOCK_PATTERN,
                tmr::WDT_UNLOCK_PATTERN_VALUE,
            );
            mmio_write(tmr_base, tmr::WDT_3_COMMAND, 2);
            mmio_write(tmr_base, tmr::TMR_8_PTV, (1 << 31) | (1 << 30));
            mmio_write(tmr_base, tmr::WDT_3_CONFIG, (1 << 15) | (1 << 4) | 8);
            mmio_write(tmr_base, tmr::WDT_3_COMMAND, 1);
        }
        4 => {
            mmio_write(
                tmr_base,
                tmr::WDT_4_UNLOCK_PATTERN,
                tmr::WDT_UNLOCK_PATTERN_VALUE,
            );
            mmio_write(tmr_base, tmr::WDT_4_COMMAND, 2);
            mmio_write(tmr_base, tmr::TMR_9_PTV, (1 << 31) | (1 << 30));
            mmio_write(tmr_base, tmr::WDT_4_CONFIG, (1 << 15) | (1 << 4) | 9);
            mmio_write(tmr_base, tmr::WDT_4_COMMAND, 1);
        }
        _ => unreachable!("cpuid returned a bad value"),
    }

    #[allow(clippy::empty_loop)]
    loop {}
}
