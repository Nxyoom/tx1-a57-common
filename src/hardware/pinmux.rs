use crate::mmio::{self, mmio_write};

use super::uart::Uart;

pub fn init_uart(base: usize, uart: Uart) {
    let init_uart = match uart {
        Uart::A => 0x00, // 0x0e4
        Uart::B => 0x10, // 0x0f4
        Uart::C => 0x20, // 0x104
        Uart::D => 0x30, // 0x114
    };

    mmio_write(base, mmio::pinmux::UART_X_TX + init_uart, 0);
    mmio_write(
        base,
        mmio::pinmux::UART_X_RX + init_uart,
        mmio::pinmux::INPUT | (1 << 3),
    );
    mmio_write(base, mmio::pinmux::UART_X_RTS + init_uart, 0);
    mmio_write(
        base,
        mmio::pinmux::UART_X_CTS + init_uart,
        mmio::pinmux::INPUT | (1 << 2),
    );
}
