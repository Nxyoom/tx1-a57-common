use crate::def_multi_reg32;

pub const UART_BASE: usize = 0x7000_6000;

def_multi_reg32!(
    THR_DLAB = 0x00,
    IER_DLAB = 0x04,
    IIR_FCR = 0x08,
    LCR = 0x0C,
    MCR = 0x10,
    LSR = 0x14,
    MSR = 0x18,
    SPR = 0x1C,
    IDRA_CSR = 0x20,
    RX_FIFO_CFG = 0x24,
    MIE = 0x28,
    VENDOR_STATUS = 0x2C,
    ASR = 0x3C
);
