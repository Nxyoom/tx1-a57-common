use crate::def_multi_reg32;

pub const MC_BASE: usize = 0x7001_9000;

def_multi_reg32!(
    SMMU_CONFIG = 0x10,
    SMMU_TLB_CONFIG = 0x14,
    SMMU_PTC_CONFIG = 0x18,
    SMMU_PTB_DATA = 0x20,
    SMMU_TLB_FLUSH = 0x30,
    SMMU_PTC_FLUSH = 0x34,
    SECURITY_CFG0 = 0x70,
    SECURITY_CFG1 = 0x74,
    SMMU_TRANSLATION_ENABLE_0 = 0x228,
    SMMU_TRANSLATION_ENABLE_1 = 0x22C,
    SMMU_TRANSLATION_ENABLE_2 = 0x230,
    SMMU_TRANSLATION_ENABLE_3 = 0x234,
    SEC_CARVEOUT_BOM = 0x670,
    SEC_CARVEOUT_SIZE_MB = 0x674,
    SEC_CARVEOUT_REG_CTRL = 0x678,
    IRAM_REG_CTRL = 0x964,
    VIDEO_PROTECT_GPU_OVERRIDE_0 = 0x984,
    VIDEO_PROTECT_GPU_OVERRIDE_1 = 0x988,
    SECURITY_CFG3 = 0x9bc,
    SMMU_TRANSLATION_ENABLE_4 = 0xb98,
    SECURITY_CARVEOUT_0_CFG = 0xbb8,
    SECURITY_CARVEOUT_0_ADDR_LOW = 0xbbc,
    SECURITY_CARVEOUT_0_ADDR_HI = 0xbc0,
    SECURITY_CARVEOUT_0_SIZE_128KB = 0xbc4,
    SECURITY_CARVEOUT_0_CLIENT_ACCESS_0 = 0xbc8,
    SECURITY_CARVEOUT_0_CLIENT_ACCESS_1 = 0xbcc,
    SECURITY_CARVEOUT_0_CLIENT_ACCESS_2 = 0xbd0,
    SECURITY_CARVEOUT_0_CLIENT_ACCESS_3 = 0xbd4,
    SECURITY_CARVEOUT_0_CLIENT_ACCESS_4 = 0xbd8,
    SECURITY_CARVEOUT_0_CLIENT_FORCE_INTERNAL_ACCESS_0 = 0xbdc,
    SECURITY_CARVEOUT_0_CLIENT_FORCE_INTERNAL_ACCESS_1 = 0xbe0,
    SECURITY_CARVEOUT_0_CLIENT_FORCE_INTERNAL_ACCESS_2 = 0xbe4,
    SECURITY_CARVEOUT_0_CLIENT_FORCE_INTERNAL_ACCESS_3 = 0xbe8,
    SECURITY_CARVEOUT_0_CLIENT_FORCE_INTERNAL_ACCESS_4 = 0xbec,
    SECURITY_CARVEOUT_1_CFG = 0xc08,
    SECURITY_CARVEOUT_1_ADDR_LOW = 0xc0c,
    SECURITY_CARVEOUT_1_ADDR_HI = 0xc10,
    SECURITY_CARVEOUT_1_SIZE_128KB = 0xc14,
    SECURITY_CARVEOUT_1_CLIENT_ACCESS_0 = 0xc18,
    SECURITY_CARVEOUT_1_CLIENT_ACCESS_1 = 0xc1c,
    SECURITY_CARVEOUT_1_CLIENT_ACCESS_2 = 0xc20,
    SECURITY_CARVEOUT_1_CLIENT_ACCESS_3 = 0xc24,
    SECURITY_CARVEOUT_1_CLIENT_ACCESS_4 = 0xc28,
    SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_0 = 0xc2c,
    SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_1 = 0xc30,
    SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_2 = 0xc34,
    SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_3 = 0xc38,
    SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_4 = 0xc3c,
    SECURITY_CARVEOUT_2_CFG = 0xc58,
    SECURITY_CARVEOUT_2_ADDR_LOW = 0xc5c,
    SECURITY_CARVEOUT_2_ADDR_HI = 0xc60,
    SECURITY_CARVEOUT_2_SIZE_128KB = 0xc64,
    SECURITY_CARVEOUT_2_CLIENT_ACCESS_0 = 0xc68,
    SECURITY_CARVEOUT_2_CLIENT_ACCESS_1 = 0xc6c,
    SECURITY_CARVEOUT_2_CLIENT_ACCESS_2 = 0xc70,
    SECURITY_CARVEOUT_2_CLIENT_ACCESS_3 = 0xc74,
    SECURITY_CARVEOUT_2_CLIENT_ACCESS_4 = 0xc78,
    SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_0 = 0xc7c,
    SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_1 = 0xc80,
    SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_2 = 0xc84,
    SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_3 = 0xc88,
    SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_4 = 0xc8c,
    SECURITY_CARVEOUT_3_CFG = 0xca8,
    SECURITY_CARVEOUT_3_ADDR_LOW = 0xcac,
    SECURITY_CARVEOUT_3_ADDR_HI = 0xcb0,
    SECURITY_CARVEOUT_3_SIZE_128KB = 0xcb4,
    SECURITY_CARVEOUT_3_CLIENT_ACCESS_0 = 0xcb8,
    SECURITY_CARVEOUT_3_CLIENT_ACCESS_1 = 0xcbc,
    SECURITY_CARVEOUT_3_CLIENT_ACCESS_2 = 0xcc0,
    SECURITY_CARVEOUT_3_CLIENT_ACCESS_3 = 0xcc4,
    SECURITY_CARVEOUT_3_CLIENT_ACCESS_4 = 0xcc8,
    SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_0 = 0xccc,
    SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_1 = 0xcd0,
    SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_2 = 0xcd4,
    SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_3 = 0xcd8,
    SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_4 = 0xcdc,
    SECURITY_CARVEOUT_4_CFG = 0xcf8,
    SECURITY_CARVEOUT_4_ADDR_LOW = 0xcfc,
    SECURITY_CARVEOUT_4_ADDR_HI = 0xd00,
    SECURITY_CARVEOUT_4_SIZE_128KB = 0xd04,
    SECURITY_CARVEOUT_4_CLIENT_ACCESS_0 = 0xd08,
    SECURITY_CARVEOUT_4_CLIENT_ACCESS_1 = 0xd0c,
    SECURITY_CARVEOUT_4_CLIENT_ACCESS_2 = 0xd10,
    SECURITY_CARVEOUT_4_CLIENT_ACCESS_3 = 0xd14,
    SECURITY_CARVEOUT_4_CLIENT_ACCESS_4 = 0xd18,
    SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_0 = 0xd1c,
    SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_1 = 0xd20,
    SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_2 = 0xd24,
    SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_3 = 0xd28,
    SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_4 = 0xd2c,
    SECURITY_CARVEOUT_5_CFG = 0xd48,
    SECURITY_CARVEOUT_5_ADDR_LOW = 0xd4c,
    SECURITY_CARVEOUT_5_ADDR_HI = 0xd50,
    SECURITY_CARVEOUT_5_SIZE_128KB = 0xd54,
    SECURITY_CARVEOUT_5_CLIENT_ACCESS_0 = 0xd58,
    SECURITY_CARVEOUT_5_CLIENT_ACCESS_1 = 0xd5c,
    SECURITY_CARVEOUT_5_CLIENT_ACCESS_2 = 0xd60,
    SECURITY_CARVEOUT_5_CLIENT_ACCESS_3 = 0xd64,
    SECURITY_CARVEOUT_5_CLIENT_ACCESS_4 = 0xd68,
    SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_0 = 0xd6c,
    SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_1 = 0xd70,
    SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_2 = 0xd74,
    SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_3 = 0xd78,
    SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_4 = 0xd7c
);
