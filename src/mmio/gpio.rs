use crate::def_multi_reg32;

pub const GPIO_BASE: usize = 0x6000_D000;

def_multi_reg32!(
    CNF = 0x00,
    OE = 0x10,
    OUT = 0x20,
    IN = 0x30,
    INT_STA = 0x40,
    INT_ENB = 0x50,
    INT_LVL = 0x60,
    INT_CLR = 0x70
);
