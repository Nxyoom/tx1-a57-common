use crate::def_multi_reg32;

pub const SYSTEM_REGISTERS_BASE: usize = 0x6000_c000;

def_multi_reg32!(
    SB_CSR = 0x00,
    SB_AA64_RESET_LOW = 0x230,
    SB_AA64_RESET_HIGH = 0x234
);
