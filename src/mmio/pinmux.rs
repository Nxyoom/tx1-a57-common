use crate::def_multi_reg32;

pub const PINMUX_BASE: usize = 0x7000_3000;

def_multi_reg32!(
    UART_X_TX = 0xE4,
    UART_X_RX = 0xE8,
    UART_X_RTS = 0xEC,
    UART_X_CTS = 0xF0
);

pub const SCHMT: u32 = 1 << 12;
pub const LPDR: u32 = 1 << 8;
pub const LOCK: u32 = 1 << 7;
pub const INPUT: u32 = 1 << 6;
pub const PARK: u32 = 1 << 5;
pub const TRISTATE: u32 = 1 << 4;
