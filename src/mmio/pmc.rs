use crate::def_multi_reg32;

pub const PMC_BASE: usize = 0x7000_E400;

def_multi_reg32!(
    APBDEV_PMC_CNTRL = 0x00,
    APBDEV_PMC_DPD_ENABLE = 0x24,
    APBDEV_PMC_PWRGATE_TOGGLE = 0x30,
    APBDEV_PMC_PWRGATE_STATUS = 0x38,
    APBDEV_PMC_SCRATCH0 = 0x50,
    APBDEV_PMC_RST_STATUS = 0x1B4,
    APBDEV_PMC_SEC_DISABLE3 = 0x2d8,
    APBDEV_PMC_SECURE_SCRATCH34 = 0x368,
    APBDEV_PMC_SECURE_SCRATCH35 = 0x36c,
    APBDEV_PMC_SCRATCH200 = 0x840
);

/// For toggle this is the bottom 5 bits, for status this is the bitshift. N didn't notice this.
#[derive(Debug, Clone, Copy)]
pub enum PwrGateDevice {
    CRail = 0,
    VideoEncode = 2,
    PCX = 3,
    MPEGEncode = 6,
    SAX = 8,
    CE1 = 9,
    CE2 = 10,
    CE3 = 11,
    CE0 = 14,
    C0NC = 15,
    SOR = 17,
    DIS = 18,
    DISB = 19,
    XUSBA = 20,
    XUSBB = 21,
    XUSBC = 22,
    VIC = 23,
    IRAM = 24,
    NVDEC = 25,
    NVJPG = 26,
    AUD = 27,
    DFD = 28,
    VE2 = 29,
}

pub const PMC_CNTRL_MAIN_RST: u32 = 1 << 4;
