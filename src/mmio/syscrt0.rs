use crate::def_multi_reg32;

pub const SYSCRT0_BASE: usize = 0x700F_0000;

def_multi_reg32!(SYSCRT0_CNTCR = 0x00, SYSCTR0_CNTFID0 = 0x20);

pub const CNTCR_ENABLE: u32 = 0b1;
pub const CNTCR_HALT_ON_DEBUG: u32 = 0b10;
