use crate::def_multi_reg32;

pub const AHB_BASE: usize = 0x6000_C000;

def_multi_reg32!(
    AHB_ARBITRATION_DISABLE = 0x04,
    AHB_ARBITRATION_PRIORITY_CTRL = 0x08,
    AHB_MASTER_SWID_0 = 0x18,
    AHB_MASTER_SWID_1 = 0x38,
    AHB_GIZMO_TZRAM = 0x54
);

pub const AHB_DISABLE_USB_ARB: u32 = 1 << 6;
pub const AHB_DISABLE_USB2_ARB: u32 = 1 << 18;

pub const AHB_GIZMO_TZRAM_DONT_SPLIT_WRITE: u32 = 1 << 7;
