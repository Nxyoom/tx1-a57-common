use crate::{el::currentel, read_spr};

#[macro_export]
macro_rules! static_panic {
    ($uart_base:expr, $message:expr) => {{
        $crate::log::write($uart_base, "[PANIC] ");
        $crate::log::write($uart_base, $message);
        $crate::log::write($uart_base, " [END OF PANIC]\n");
        panic!();
    }};
}

#[no_mangle]
pub extern "C" fn check_panic() -> bool {
    let esr = match currentel() {
        crate::el::ExceptionLevel::El0 => 0,
        crate::el::ExceptionLevel::El1 => read_spr!("ESR_EL1"),
        crate::el::ExceptionLevel::El2 => read_spr!("ESR_EL2"),
        crate::el::ExceptionLevel::El3 => read_spr!("ESR_EL3"),
    };

    // TODO: Log error message via UART

    (esr >> 26) & 0b111111 == 0x3c
}
