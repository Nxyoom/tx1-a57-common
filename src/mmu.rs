// We wrap translation tables in structs because we can force alignment outside of the linker script which is nicer

use core::arch::asm;

#[repr(C)]
#[repr(align(1024))]
pub struct Level1Table(pub [u64; 0x40 / 8]);

#[repr(C)]
#[repr(align(4096))]
pub struct Level2Table(pub [u64; 0x1000 / 8]);

#[repr(C)]
#[repr(align(4096))]
pub struct Level3Table(pub [u64; 0x1000 / 8]);

pub const ATTR_BLOCK: u64 = 0b01;
pub const ATTR_PAGE: u64 = 0b11;
pub const ATTR_TABLE: u64 = 0b11;

pub const ATTR_EL0_DENIED: u64 = 0 << 6;
pub const ATTR_EL0_ALLOWED: u64 = 1 << 6;

pub const ATTR_RW: u64 = 0 << 7;
pub const ATTR_RO: u64 = 1 << 7;
pub const ATTR_NX: u64 = 1 << 54;

pub const ATTR_ACCESSED: u64 = 1 << 10;

pub const ATTR_OUTER_SHARABLE: u64 = 2 << 8;
pub const ATTR_INNER_SHARABLE: u64 = 3 << 8;

pub const ATTR_NON_SECURE: u64 = 1 << 5;

pub const ATTR_NORMAL_MEM: u64 = 0 << 2;
pub const ATTR_MMIO_MEM: u64 = 1 << 2;
pub const ATTR_UNCACHED_MEM: u64 = 3 << 2;

pub fn map_lvl1_block(lvl1: &mut Level1Table, address: u64) {
    lvl1.0[((address >> 0x1e) & 0x1ff) as usize] = address
        | ATTR_ACCESSED
        | ATTR_INNER_SHARABLE
        | ATTR_RW
        | ATTR_EL0_DENIED
        | ATTR_NON_SECURE
        | ATTR_MMIO_MEM
        | ATTR_BLOCK;
}

pub fn map_lvl1_dtable(lvl1: &mut Level1Table, lvl2: &mut Level2Table, page: u64) {
    let adjusted = page >> 0x1e & 0x1ff;
    let lvl2_tbl_ptr = &(lvl2.0[0]) as *const _ as u64;
    lvl1.0[adjusted as usize] = lvl2_tbl_ptr | ATTR_TABLE;
}

pub fn map_lvl2_dtable(lvl2: &mut Level2Table, lvl3: &mut Level3Table, page: u64) {
    let adjusted = page >> 0x15 & 0x1ff;
    let lvl3_tbl_ptr = &(lvl3.0[0]) as *const _ as u64;
    lvl2.0[adjusted as usize] = lvl3_tbl_ptr | ATTR_TABLE;
}

/// Maps a page in the lvl3 TT
///
/// `ATTR_ACCESSED` and `ATTR_PAGE` are automatically added
pub fn map_lvl3_page(
    lvl3: &mut Level3Table,
    virtual_address: u64,
    physical_address: u64,
    length: u64,
    attributes: u64,
) {
    let mut adjusted_len = ((length / 0x1000) & 0xFFFF_FFFF) as u32;
    let mut adjusted_va = (virtual_address / 0x1000) & 0x1ff;
    let mut adjusted_pa = ((physical_address / 0x1000) & 0xFFFFFFFFF) << 0xc;
    while adjusted_len != 0 {
        lvl3.0[adjusted_va as usize] = adjusted_pa | attributes | ATTR_ACCESSED | ATTR_PAGE;

        adjusted_pa += 0x1000;
        adjusted_va += 1;
        adjusted_len -= 1;
    }
}

pub fn configure_registers(lvl1: &Level1Table) {
    let tt_base = &lvl1.0[0] as *const _ as u64;

    let mut mair = 0u64;
    mair |= 0b1111_1111; // Attr 0 = Normal memory; Device-GRE; Outer Write-Back Non-transient and allocate on both RW; Inner Write-Back Non-transient and allocate on both RW;
    mair |= (0b0000_0100) << 8; // Attr 1 = Device memory; Device-nGnRE;
    mair |= (0b0000_0000) << 16; // Attr 2 = Device memory; Device-nGnRnE;

    let mut scr = 0u64;
    scr |= 1 << 0; // ELs lower than 3 are non-secure
    scr |= 0 << 1; // IRQ goes to EL3
    scr |= 1 << 2; // FIQ goes to EL3
    scr |= 1 << 3; // SError and External Aborts go to EL3
    scr |= (1 << 4) | (1 << 5); // RES1
    scr |= 1 << 9; // Enable Secure Instruction Fetch (cannot execute from non-secure pages when in secure-mode)
    scr |= 1 << 10; // Next level is A64

    unsafe {
        asm!(
            "MSR s3_1_c15_c2_1, {s3_1_c15_c2_1_value}",
            "ISB",
            "MSR SCR_EL3, {scr_value}",
            "MSR TTBR0_EL3, {ttbr0_value}",
            "MSR TCR_EL3, {tcr_value}",
            "MSR CPTR_EL3, {cptr_value}",
            "MSR MAIR_EL3, {mair_value}",
            "ISB",
            "DSB ISH",
            "TLBI ALLE3IS",
            "DSB ISH",
            "ISB",
            "MSR SCTLR_EL3, {sctlr_value}",
            "ISB",
            s3_1_c15_c2_1_value = in(reg) 0x1b00000040u64,
            scr_value = in(reg) scr,
            ttbr0_value = in(reg) tt_base,
            tcr_value = in(reg) 0x8081351fu64,
            cptr_value = in(reg) 0u64,
            mair_value = in(reg) mair,
            sctlr_value = in(reg) 0x30c51835u64,
        );
    }
}
