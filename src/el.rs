use core::arch::asm;

use crate::{read_spr, static_panic};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum ExceptionLevel {
    /// User-mode
    El0,
    /// Kernel
    El1,
    /// Hypervisor
    El2,
    /// Secure Monitor
    El3,
}

#[inline(always)]
pub fn currentel() -> ExceptionLevel {
    match (read_spr!("CurrentEL") >> 2) & 0b11 {
        0 => ExceptionLevel::El0,
        1 => ExceptionLevel::El1,
        2 => ExceptionLevel::El2,
        _ => ExceptionLevel::El3,
    }
}

pub fn set_elr(uart_base: usize, location: *const u8) {
    match currentel() {
        ExceptionLevel::El0 => static_panic!(uart_base, "[A57C] Attempted to set ELR in EL0"),
        ExceptionLevel::El1 => unsafe {
            asm!("msr elr_el1, {location}", location = in(reg) location)
        },
        ExceptionLevel::El2 => unsafe {
            asm!("msr elr_el2, {location}", location = in(reg) location)
        },
        ExceptionLevel::El3 => unsafe {
            asm!("msr elr_el3, {location}", location = in(reg) location)
        },
    }
}
